/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author Joao
 */
public class Retangulo extends Poligono{
    double lado1, lado2;

    public Retangulo(double lado1, double lado2) {
        this.lado1 = lado1;
        this.lado2 = lado2;
    }
    
    public double getLadoMenor() {
        if (lado1>lado2)
            return lado2;
        else
            return lado1;
    }

    public double getLadoMaior() {
        if (lado1>lado2) {
            return lado1;
        } else {
            return lado2;
        }
    }

    @Override
    public double getPerimetro() {
        return (lado1+lado2)*2;
    }

    @Override
    public double getArea() {
        return (lado1*lado2);
    }
    
}
