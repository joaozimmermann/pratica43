package utfpr.ct.dainf.if62c.pratica;
public class Circulo extends Elipse {

    public Circulo(double r) {
        super(r, r);
    }

    @Override
    public double getPerimetro() {
        double a = Math.PI*2*getEixoMaior();
        return a;
    }
    
}
