/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author Joao
 */
public class TrianguloEquilatero extends Retangulo{

    public TrianguloEquilatero(double lado) {
        super(lado, lado);
    }
    
    @Override
    public double getLadoMenor() {
        return lado1;
    }

    @Override
    public double getLadoMaior() {
        return lado1;
    }

    @Override
    public double getPerimetro() {
        return lado1*3;
    }

    @Override
    public double getArea() {
        return (Math.pow(lado1, 2)*Math.sqrt(3))/4;
    }
    
}
