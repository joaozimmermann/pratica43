package utfpr.ct.dainf.if62c.pratica;

import java.io.Serializable;

/**
 * Programação em Java.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public abstract class Poligono implements FiguraComLados, Serializable {

    
    /**
     * Retorna o nome da figura.
     * Este método retorno o nome não qualificado da classe.
     * @return O nome da figura.
     */
    @Override
    public String getNome() {
        return this.getClass().getSimpleName();
    }

    @Override
    public String toString() {
        return getNome();
    }

}
