package utfpr.ct.dainf.if62c.pratica;

public class Elipse implements FiguraComEixos {
    private double r;
    private double s;

    public Elipse(double eixo1, double eixo2) {
        this.r = eixo1;
        this.s = eixo2;
    }
    
    public void setR(double r) {
        this.r = r;
    }

    public void setS(double s) {
        this.s = s;
    }

    public double getR() {
        return r;
    }

    public double getS() {
        return s;
    }

    @Override
    public double getArea(){
        double a = Math.PI*getEixoMaior()*getEixoMenor();
        return a;
    }
    
    @Override
    public double getPerimetro(){
        double a = Math.PI*((3*(getEixoMaior()+getEixoMenor()))-Math.sqrt(((3*getEixoMaior()+getEixoMenor())*(getEixoMaior()+3*getEixoMenor()))));
        return a;
    }

    @Override
    public double getEixoMenor() {
        if(r>s)
            return s;
        else
            return r;
    }

    @Override
    public double getEixoMaior() {
        if(r>s)
            return r;
        else
            return s;
    }

    @Override
    public String getNome() {
        return "Elipse";
    }
    
}
