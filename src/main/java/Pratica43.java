
import utfpr.ct.dainf.if62c.pratica.Quadrado;
import utfpr.ct.dainf.if62c.pratica.Retangulo;
import utfpr.ct.dainf.if62c.pratica.TrianguloEquilatero;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Joao
 */
public class Pratica43 {
    public static void main(String[] args) {
        Retangulo r1 = new Retangulo(2, 4);
        System.out.println(r1.getPerimetro());
        System.out.println(r1.getArea());
        TrianguloEquilatero t1 = new TrianguloEquilatero(3);
        System.out.println(t1.getArea());
        System.out.println(t1.getPerimetro());
        Quadrado q1 = new Quadrado(5);
        System.out.println(q1.getArea());
        System.out.println(q1.getPerimetro());
    }
}
